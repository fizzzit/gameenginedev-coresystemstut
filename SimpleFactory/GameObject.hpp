#ifndef _GAMEOBJECT_HPP_
#define _GAMEOBJECT_HPP_

class GameObject
{
    public:
    GameObject();
    virtual ~GameObject();
    
    enum Type
    {
        NPC,
        ROCKET,
        //New types go in here. 
        NUM_TYPES
    };
    //This last bit is quite clever. 

    /* Pure Virtuals */
    virtual void draw() = 0;
    virtual void update() = 0;
};

#endif
